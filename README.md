These templates include:

PCB Related:
* Board stackup
    - Material parameters
    - Thickness
    - Colors
* Default Solder Mask/Paste parameters
* Test & Graphics, 0.15mm thick/0.6mm width/height for all non-copper layers
* Text Variable for REFERENCE\_NUMBER set to XXXX-YYYYYYYYYY-ZZZZZ
* All applicable design rule constraints, updated for KiCAD 6
* Net Classes
    - Default
    - CBCPW (Conductor Backed Co-Planar Waveguide)
        + A reasonable 50 ohm unbalanced line
        + A reasonable 100 ohm balanced line
    - Minimum CBCPW, the smallest version of above
    - Power

Project Related:
* Symbol libraries imported:
    - 000\_PreferredDiodes
    - 000\_PreferredParts
    - POWDER\_Analog
    - POWDER\_Connector
    - POWDER\_Data\_Converter
    - POWDER\_Digital
    - POWDER\_ESD
    - POWDER\_Isolation
    - POWDER\_MCU
    - POWDER\_Misc
    - POWDER\_Power
    - POWDER\_PowerGlobals
    - POWDER\_RF
* Footprint libraries imported:
    - POWDER\_Analog
    - POWDER\_Connector
    - POWDER\_Digital
    - POWDER\_ESD
    - POWDER\_Heatsink
    - POWDER\_MCU
    - POWDER\_Misc
    - POWDER\_PCBEdge
    - POWDER\_Power
    - POWDER\_RF
    - POWDER\_Specific

A meta directory to be used as a template.
